package api_common

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func ErrResp(c *gin.Context, code int, msg string, data interface{}) {
	c.JSON(code, gin.H{"code": http.StatusOK, "msg": msg, "data": data})
	return
}
