package lms_api

import (
	"net/http"
	"time"

	"gitee.com/onpromise/lms/api/api_common"
	"gitee.com/onpromise/lms/consts"
	"gitee.com/onpromise/lms/models/db/lms_db"
	"gitee.com/onpromise/lms/models/rest/lms_rest"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/copier"
	"gorm.io/gorm"
)

//lmsSummary 向各个signer发起请求, 获取每种具体授权的信息
//lmsSummary也会在它自己的数据库中缓存一份数据, 如果哪个lms突然请求不了, 那么只返回缓存的数据
func lmsSummary(c *gin.Context) {
	ctxdb := c.Value(consts.CTXDB).(*gorm.DB)
	licenseCounts := []lms_db.LicenseCount{}
	err := ctxdb.Find(&licenseCounts).Error
	if err != nil {
		api_common.ErrResp(c, http.StatusInternalServerError, "授权数据查询失败", gin.H{"error_detail": err.Error()})
		return
	}
	restCounts := []lms_rest.LicenseCount{}
	totalCount := lms_rest.LicenseCount{}
	lms_db.LicenseCounts(licenseCounts).Copy(func(count lms_db.LicenseCount) {
		restCount := lms_rest.LicenseCount{}
		copier.Copy(&restCount, &count)
		restCounts = append(restCounts, restCount)
	})
	dbTotal := lms_db.LicenseCounts(licenseCounts).Sum()
	copier.Copy(&dbTotal, &totalCount)
	respData := lms_rest.SummaryRespData{Summary: lms_rest.LicenseSummary{
		CountsBySigner: restCounts,
		CountsTotal:    totalCount,
		UpdatedAt:      time.Now().UTC().Format(time.RFC3339),
	}}
	resp := lms_rest.SummaryResp{Code: http.StatusOK, Msg: "", Data: respData}
	c.JSON(http.StatusOK, resp)
}
