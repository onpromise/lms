package consts

type ProtectType string

const (
	CTXDB             = "ctxdb"
	ENCRYTP_ALG       = "encrypt_alg"
	FOUNDATION_BLOCK  = "foundation_block"
	PACKAGE_BLOCK     = "package_block"
	LICENSE_BLOCK     = "license_block"
	LMS_PKG_TABLENAME = "lms_package_dbs"
)
const (
	PROTECT_VWAF      ProtectType = "vWAF"      //web防护
	PROTECT_VFW       ProtectType = "vFW"       //firewall
	PROTECT_VSCAN     ProtectType = "vSCAN"     //漏扫
	PROTECT_VBASTION  ProtectType = "vBASTION"  //堡垒
	PROTECT_VDBAUDIT  ProtectType = "vDBAUDIT"  //数据库审计
	PROTECT_VLOGAUDIT ProtectType = "vLOGAUDIT" //日志审计
)
