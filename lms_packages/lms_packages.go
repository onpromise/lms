package lms_packages

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitee.com/onpromise/lms/common"
	"gitee.com/onpromise/lms/config"
	"gitee.com/onpromise/lms/http/httpclient"
	"gitee.com/onpromise/lms/lib/signchain"
	"gitee.com/onpromise/lms/models/db/lms_db"
	"gitee.com/onpromise/lms/models/rest/lms_rest"
	"github.com/lwydyby/logrus"
	"gorm.io/gorm"
)

// 1. 向各个signer确认subpackage的合法性
// 2. 将总包存入临时表/文件等
// 3. 将各个subpackage存入各个signer的chaindb
// 4. 将总包存入总包的chaindb
// 如果中间一步失败了, 需要撤销
func Import(ctx context.Context, pkg lms_rest.LMSPackage) error {
	db, err := common.GetCtxDB(ctx)
	if err != nil {
		logrus.Error(err)
		return err
	}
	signservice := signchain.CreateSignChainService(ctx)
	dbPkg := lms_db.LMSPackageDB{PackageID: pkg.PackageId, Package: lms_db.LMSPackage(pkg)}
	err = db.Transaction(func(tx *gorm.DB) error {
		deadline, err := time.Parse(time.RFC3339, pkg.ImportDeadline)
		if err != nil {
			logrus.WithField("package_id", pkg.PackageId).Error(err)
			return err
		}
		for _, subPkg := range pkg.SubPackages {
			err := ImportSubpkgReq(ctx, subPkg)
			if err != nil {
				logrus.WithField("package_id", pkg.PackageId).Error(err)
				return err
			}
		}
		bs, _ := json.Marshal(pkg)
		signservice.AppendPackageBlock(deadline, string(bs))
		if err := tx.Create(&dbPkg).Error; err != nil {
			logrus.WithField("package_id", pkg.PackageId).Error(err)
			return err
		}
		return nil
	}, nil)
	return err
}
func ImportSubpkgReq(ctx context.Context, pkg lms_rest.SubPackage) error {
	pkgType := pkg.ProtectType
	endpoint, token, err := config.GetConfig().GetSignServiceEndpoints(pkgType)
	if err != nil {
		logrus.Error(err)
		return err
	}
	headers := map[string]string{
		"Authorization": token,
	}
	bs, _ := json.Marshal(pkg)
	code, resp, err := httpclient.Post(endpoint, bytes.NewReader(bs), headers)
	if err != nil {
		logrus.Error(err)
		return err
	}
	if code < 200 || code >= 300 {
		err := fmt.Errorf("code is %d,resp body is:%s", code, string(resp))
		logrus.Error(err)
		return err
	}
	return nil
}
func ImportSubpackage(ctx context.Context, pkg lms_rest.SubPackage) error {
	db, err := common.GetCtxDB(ctx)
	if err != nil {
		logrus.Error(err)
		return err
	}
	signservice := signchain.CreateSignChainService(ctx)
	dbPkg := lms_db.LMSSubPackageDB{PackageID: pkg.PackageId, Package: lms_db.LMSSubPackage(pkg)}
	err = db.Transaction(func(tx *gorm.DB) error {
		deadline, err := time.Parse(time.RFC3339, pkg.ImportDeadline)
		if err != nil {
			logrus.WithField("package_id", pkg.PackageId).Error(err)
			return err
		}
		bs, _ := json.Marshal(pkg)
		signservice.AppendPackageBlock(deadline, string(bs))
		if err := tx.Create(&dbPkg).Error; err != nil {
			logrus.WithField("package_id", pkg.PackageId).Error(err)
			return err
		}
		return nil
	}, nil)
	return err
}
