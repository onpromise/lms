package lms_api

import "github.com/gin-gonic/gin"

func Routes(r *gin.RouterGroup) *gin.RouterGroup {
	r.GET("/summary", lmsSummary)
	r.POST("/licenses", licenseApply)
	r.POST("/packages", licenseApply)
	r.POST("/licenses", licenseApply)
	r.GET("/licenses", licenseList)
	r.DELETE("/licenses/:licenseid", licenseRevert)
	r.PUT("/licenses/:licenseid", licenseUpdate)
	r.GET("/licenses/:licenseid", licenseGet)
	r.POST("/packages", importPackage) //导入新的授权包
	return r
}
