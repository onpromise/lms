// Code generated by github.com/atombender/go-jsonschema, DO NOT EDIT.

package lms_rest

type LMSPackage struct {
	// 授权包创建的时间
	CreatedAt string `json:"created_at,omitempty" yaml:"created_at,omitempty"`

	// 此次导入的概要信息,例如描述下意图等等
	Description string `json:"description,omitempty" yaml:"description,omitempty"`

	// Detail corresponds to the JSON schema field "detail".
	Detail PackageDetail `json:"detail,omitempty" yaml:"detail,omitempty"`

	// 授权包导入截止时间
	ImportDeadline string `json:"import_deadline,omitempty" yaml:"import_deadline,omitempty"`

	// 签名加密后的数据,base64码方式存储
	PackageData string `json:"package_data,omitempty" yaml:"package_data,omitempty"`

	// PackageId corresponds to the JSON schema field "package_id".
	PackageId string `json:"package_id,omitempty" yaml:"package_id,omitempty"`

	// 总包的签名
	Signature string `json:"signature,omitempty" yaml:"signature,omitempty"`

	// SubPackages corresponds to the JSON schema field "sub_packages".
	SubPackages []SubPackage `json:"sub_packages,omitempty" yaml:"sub_packages,omitempty"`

	// Append(追加), Overwrite(重置)
	WriteMode string `json:"write_mode,omitempty" yaml:"write_mode,omitempty"`
}

// LMS的授权总包
type LmsPackageJson map[string]interface{}

// 记录这次主要操作的信息
type PackageDetail struct {
	// 增加可用组件数, 暂时不考虑将这部分的权限收归LMS
	AddCountLimit int `json:"add_count_limit,omitempty" yaml:"add_count_limit,omitempty"`

	// 一个授权代表一个月的使用期,最小单位是月
	AddLicense int `json:"add_license,omitempty" yaml:"add_license,omitempty"`

	// 授权最大申请时长,如果为空就代表不改变,示例如12m,36m等等
	MaxDuration string `json:"max_duration,omitempty" yaml:"max_duration,omitempty"`
}

// 子包信息
type SubPackage struct {
	// 授权包创建的时间
	CreatedAt string `json:"created_at,omitempty" yaml:"created_at,omitempty"`

	// Description corresponds to the JSON schema field "description".
	Description string `json:"description,omitempty" yaml:"description,omitempty"`

	// Detail corresponds to the JSON schema field "detail".
	Detail PackageDetail `json:"detail,omitempty" yaml:"detail,omitempty"`

	// 授权包导入截止时间
	ImportDeadline string `json:"import_deadline,omitempty" yaml:"import_deadline,omitempty"`

	// 签名加密后的数据,base64码方式存储
	PackageData string `json:"package_data,omitempty" yaml:"package_data,omitempty"`

	// 子包的UUID
	PackageId string `json:"package_id,omitempty" yaml:"package_id,omitempty"`

	// 包提供者的UUID
	PackageVendorId string `json:"package_vendor_id,omitempty" yaml:"package_vendor_id,omitempty"`

	// 防护类型, 例如firewall,waf,ips等等
	ProtectType string `json:"protect_type,omitempty" yaml:"protect_type,omitempty"`

	// 子包的签名
	Signature string `json:"signature,omitempty" yaml:"signature,omitempty"`
}
