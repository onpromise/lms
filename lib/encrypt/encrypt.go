package encrypt

//输出结果都是base64编码的字串
type EncryptAlg interface {
	PubEncrypt([]byte) (string, error)
	PriEncrypt([]byte) (string, error)
	PriDecrypt(string) ([]byte, error)
	PubDecrypt(string) ([]byte, error)
	PriEncryptSign([]byte) (string, error) //加密,加盐,hash后签名,出结果
	VerifySign([]byte, string) error
}
type EncryptKey interface {
	HasPublic() bool
	HasPrivate() bool
	GetPrivPEM() string
	GetPubPEM() string
	GetPrivKey() interface{}
	GetPubKey() interface{}
}
