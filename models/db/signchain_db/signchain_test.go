package signchain_db

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"os"
	"strings"
	"testing"
	"time"

	"gitee.com/onpromise/lms/lib/encrypt"
	"github.com/google/uuid"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

const (
	privateKeyPrefix = "RSA PRIVATE KEY "
	publicKeyPrefix  = "RSA PUBLIC KEY "
)

var db *gorm.DB
var privKey, pubKey string

type TName struct {
	gorm.Model
	Name string
	Age  int
}

func (t TName) TableName() string {
	return "test_name"
}

func init() {
	db, _ = gorm.Open(sqlite.Open("file::memory:?cache=shared"))
	db.AutoMigrate(&SignChainBlock{}, &TName{})
}

func genPrivkeyPem(key *rsa.PrivateKey) string {
	x509PrivateKey := x509.MarshalPKCS1PrivateKey(key)
	privateBlock := pem.Block{
		Type:  privateKeyPrefix,
		Bytes: x509PrivateKey,
	}
	bs := pem.EncodeToMemory(&privateBlock)
	return string(bs)
}
func genPubkeyPem(key rsa.PublicKey) string {
	x509PublicKey, _ := x509.MarshalPKIXPublicKey(&key)
	publicBlock := pem.Block{
		Type:  publicKeyPrefix,
		Bytes: x509PublicKey,
	}
	bs := pem.EncodeToMemory(&publicBlock)
	return string(bs)
}
func setup() {
	rsaPrivKey, _ := rsa.GenerateKey(rand.Reader, 2048)
	privKey = genPrivkeyPem(rsaPrivKey)
	pubKey = genPubkeyPem(rsaPrivKey.PublicKey)
}
func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}

func TestBlockCreate(t *testing.T) {
	uuid := uuid.New().String()
	chainBlock := SignChainBlock{SignedAt: time.Now().UTC(), SignOP: LICENSE_CREATE, UUID: uuid}
	err := db.Create(&chainBlock).Error
	if err != nil {
		t.Fatal(err)
	}
	find := SignChainBlock{}
	err = db.First(&find).Error
	if err != nil {
		t.Fatal(err)
	}
	if find.UUID != uuid {
		t.Fatal("sign chain block create query failed")
	}
	if find.SignOP != LICENSE_CREATE {
		t.Fatal("wrong license operation")
	}
}
func TestChainCreate(t *testing.T) {
	now := time.Now().UTC()
	start := now
	parent := ParentBlock{}
	for i := 0; i <= 100; i++ {
		signedAt := now.Add(5 * time.Second)
		uuid := uuid.New().String()
		chainBlock := SignChainBlock{SignedAt: signedAt, SignOP: LICENSE_CREATE, UUID: uuid, Parent: parent}
		parent.SignedAt = signedAt
		parent.UUID = uuid
		parent.SignOP = LICENSE_CREATE
		db.Create(&chainBlock)
		now = signedAt
	}
	chain := SignChain{Max: 5}
	chain.Load(db)

	if len(chain.Blocks) != 5 {
		t.Fatal("has not enough blocks")
	}
	if chain.Blocks[1].SignedAt.Sub(chain.Blocks[0].SignedAt) >= 0 {
		t.Fatal("first block should be the latest one")
	}
	last := chain.Blocks[4].SignedAt
	if last.Sub(start).Seconds() != 485 {
		t.Fatal("wrong time")
	}
	chain2 := SignChain{BeginAt: start.Add(490 * time.Second)}
	chain2.Load(db)
	if len(chain2.Blocks) != 4 {
		t.Fatal("has not enough blocks")
	}
}

func TestChainCheck(t *testing.T) {
	var count int64
	chainBlock := SignChainBlock{}
	db.Delete(&chainBlock, "deleted_at is null")
	db.Model(&chainBlock).Count(&count)
	if count != 0 {
		t.Fatal("delete failed")
	}
	rsaEn, err := encrypt.NewRsaEncrypt([]byte(privKey))
	if err != nil {
		t.Fatal(err)
	}
	now := time.Now().UTC()
	//start := now
	parent := ParentBlock{}
	for i := 0; i < 100; i++ {
		signedAt := now.Add(5 * time.Second)
		uuid := uuid.New().String()
		chainBlock := SignChainBlock{SignedAt: signedAt, SignOP: LICENSE_CREATE, UUID: uuid, Parent: parent}
		chainBlock.SignType = LICENSE_MANAGE_TYPE
		parent.SignedAt = signedAt
		parent.UUID = uuid
		parent.SignOP = LICENSE_CREATE
		parent.SignType = chainBlock.SignType
		chainBlock.Sign(rsaEn)
		db.Create(&chainBlock)
		now = signedAt
	}
	db.Model(&chainBlock).Count(&count)
	if count != 100 {
		t.Fatal("create failed")
	}
	chain := SignChain{Max: 100}
	chain.Load(db)

	if len(chain.Blocks) != 100 {
		t.Fatal("has not enough blocks")
	}
	if err := chain.Verify(rsaEn); err != nil {
		t.Fatal(err)
	}
}
func TestGuardBlock(t *testing.T) {
	var count int64
	chainBlock := SignChainBlock{}
	db.Delete(&chainBlock, "deleted_at is null")
	db.Model(&chainBlock).Count(&count)
	if count != 0 {
		t.Fatal("delete failed")
	}
	rsaEn, err := encrypt.NewRsaEncrypt([]byte(privKey))
	if err != nil {
		t.Fatal(err)
	}
	now := time.Now().UTC()
	//start := now
	parent := ParentBlock{}
	for i := 0; i < 100; i++ {
		signType := LICENSE_MANAGE_TYPE
		if i%10 == 0 {
			signType = LICENSE_GUARD_TYPE
		}
		signedAt := now.Add(5 * time.Second)
		uuid := uuid.New().String()
		chainBlock := SignChainBlock{SignedAt: signedAt, SignOP: LICENSE_CREATE, UUID: uuid, Parent: parent}
		chainBlock.SignType = signType
		parent.SignedAt = signedAt
		parent.UUID = uuid
		parent.SignOP = LICENSE_CREATE
		parent.SignType = chainBlock.SignType
		chainBlock.Sign(rsaEn)
		db.Create(&chainBlock)
		now = signedAt
	}
	db.Model(&chainBlock).Count(&count)
	if count != 100 {
		t.Fatal("create failed")
	}
	chain := SignChain{Max: 100}
	chain.Load(db)

	if len(chain.Blocks) != 100 {
		t.Fatal("has not enough blocks")
	}
	if err := chain.Verify(rsaEn); err != nil {
		t.Fatal(err)
	}
}

//sytax error in sqlite3
func TestTableLock(t *testing.T) {
	err := db.Exec("LOCK TABLE test_name write").Error
	//error for sqlite,not for mariadb
	if strings.Contains(err.Error(), `near "LOCK": syntax error`) {
		t.Log(err)
	} else {
		t.Fatal(err)
	}
}
func TestFindLatest(t *testing.T) {
	blocks := []SignChainBlock{}
	err := db.Order("signed_at desc").Limit(2).Find(&blocks).Error
	if err != nil {
		t.Fatal(err)
	}
	t.Log(blocks[0].SignedAt)
	t.Log(blocks[1].SignedAt)
	if blocks[0].SignedAt.Sub(blocks[1].SignedAt) < 0 {
		t.Fatal("find latest failed")
	}
	if len(blocks) > 2 {
		t.Fatal("find latest failed")
	}
	chain := SignChain{Max: 2}
	chain.Load(db)
	if len(chain.Blocks) > 2 {
		t.Fatal("find latest failed")
	}
	if chain.Blocks[0].SignedAt != blocks[0].SignedAt {
		t.Fatal("load failed")
	}
	if chain.Blocks[1].SignedAt != blocks[1].SignedAt {
		t.Fatal("load failed")
	}

}
