package lms_db

import "gorm.io/gorm"

func Migrate(db *gorm.DB) error {
	return db.AutoMigrate(&LicenseCount{}, &License{})
}
