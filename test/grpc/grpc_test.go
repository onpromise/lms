package grpc_test

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"log"
	"math/big"
	"net/http"
	"os"
	"testing"
	"time"

	"gitee.com/onpromise/lms/models/grpc/protogen"
	"github.com/gin-gonic/gin"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

var server *grpc.Server

const (
	rootkeyFile   = "/tmp/test.root.key"
	clientkeyFile = "/tmp/test.client.key"
	caFile        = "/tmp/test.ca.crt"
)

type rpcServer struct {
	protogen.UnimplementedTestKVServiceServer
}

func (*rpcServer) Get(ctx context.Context, req *protogen.TestKeyValue) (*protogen.TestKeyValue, error) {
	resp := protogen.TestKeyValue{Key: "test key", Value: "test value"}
	return &resp, nil
}
func setupHttp() {
	rpc := rpcServer{}
	server = grpc.NewServer()
	protogen.RegisterTestKVServiceServer(server, &rpc)
	r := gin.New()
	r.Use(func(c *gin.Context) {
		if c.Request.ProtoMajor == 2 && c.GetHeader("Content-Type") == "application/grpc" {
			c.Status(http.StatusOK)
			log.Println(c.Request.Header)
			log.Println(c.GetHeader("Auth"))
			server.ServeHTTP(c.Writer, c.Request)
			c.Abort()
			return
		}
		c.Next()
	})
	h2Handle := h2c.NewHandler(r.Handler(), &http2.Server{})
	server := &http.Server{
		Addr:    "0.0.0.0:18880",
		Handler: h2Handle,
	}
	server.ListenAndServe()
}
func generateCert() {
	rootKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		panic(err)
	}
	keyToFile(rootkeyFile, rootKey)
	clientKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		panic(err)
	}
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		panic(err)
	}

	keyToFile(clientkeyFile, clientKey)
	rootTemplate := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Root"},
			CommonName:   "Root CA",
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(0, 0, 7),
		KeyUsage:              x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
		IsCA:                  true,
	}

	clientTemplate := x509.Certificate{
		SerialNumber: new(big.Int).SetInt64(4),
		Subject: pkix.Name{
			Organization: []string{"public lms"},
			CommonName:   "lms.gitee",
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(0, 0, 1),
		KeyUsage:              x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth},
		BasicConstraintsValid: true,
		IsCA:                  false,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &clientTemplate, &rootTemplate, &clientKey.PublicKey, rootKey)
	if err != nil {
		panic(err)
	}
	certToFile(caFile, derBytes)

}

//https://github.com/Shyp/generate-tls-cert/blob/master/generate.go
func keyToFile(filename string, key *ecdsa.PrivateKey) {
	file, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	b, err := x509.MarshalECPrivateKey(key)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to marshal ECDSA private key: %v", err)
		os.Exit(2)
	}
	if err := pem.Encode(file, &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}); err != nil {
		panic(err)
	}
}

func certToFile(filename string, derBytes []byte) {
	certOut, err := os.Create(filename)
	if err != nil {
		log.Fatalf("failed to open cert.pem for writing: %s", err)
	}
	if err := pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes}); err != nil {
		log.Fatalf("failed to write data to cert.pem: %s", err)
	}
	if err := certOut.Close(); err != nil {
		log.Fatalf("error closing cert.pem: %s", err)
	}
}
func setupHttps() {
	rpc := rpcServer{}
	server = grpc.NewServer()
	protogen.RegisterTestKVServiceServer(server, &rpc)
	r := gin.New()
	r.Use(func(c *gin.Context) {
		if c.Request.ProtoMajor == 2 && c.GetHeader("Content-Type") == "application/grpc" {
			c.Status(http.StatusOK)
			log.Println(c.Request.Header)
			log.Println(c.GetHeader("Auth"))
			server.ServeHTTP(c.Writer, c.Request)
			c.Abort()
			return
		}
		c.Next()
	})
	r.RunTLS("0.0.0.0:18443", caFile, clientkeyFile)
}
func setup() {
	generateCert()
	go setupHttp()
	go setupHttps()
}
func shutdown() {
	os.Remove(rootkeyFile)
	os.Remove(clientkeyFile)
	os.Remove(caFile)
}
func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func TestHttpClient(t *testing.T) {
	conn, err := grpc.Dial("localhost:18880", grpc.WithInsecure(), grpc.WithTimeout(3*time.Second))
	if err != nil {
		t.Fatal(err)
	}
	client := protogen.NewTestKVServiceClient(conn)
	keyval := protogen.TestKeyValue{Key: "client", Value: "client value"}
	ctx := context.TODO()
	md := metadata.New(map[string]string{"X-Auth": "req-123", "Auth": "123456"})
	ctx = metadata.NewOutgoingContext(ctx, md)
	var header metadata.MD
	resp, err := client.Get(ctx, &keyval, grpc.Header(&header))
	bs, _ := json.Marshal(resp)
	m := map[string]string{}
	json.Unmarshal(bs, &m)
	if m["Key"] != "test key" {
		t.Fatal("http grpc test failed")
	}
}
func TestHttpsClient(t *testing.T) {
	conn, err := grpc.Dial("localhost:18443", grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{InsecureSkipVerify: true})), grpc.WithTimeout(3*time.Second))
	if err != nil {
		t.Fatal(err)
	}
	client := protogen.NewTestKVServiceClient(conn)
	keyval := protogen.TestKeyValue{Key: "client", Value: "client value"}
	ctx := context.TODO()
	md := metadata.New(map[string]string{"X-Auth": "req-123", "Auth": "123456"})
	ctx = metadata.NewOutgoingContext(ctx, md)
	var header metadata.MD
	resp, err := client.Get(ctx, &keyval, grpc.Header(&header))
	if err != nil {
		t.Fatal(err)
	}
	bs, _ := json.Marshal(resp)
	m := map[string]string{}
	json.Unmarshal(bs, &m)
	if m["Key"] != "test key" {
		t.Fatal("http grpc test failed")
	}
}
