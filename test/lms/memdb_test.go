//记录下memdb的使用, 其它的测试以此为模板来使用内存数据库
package lms

import (
	"testing"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var db *gorm.DB

type COVID19 struct {
	gorm.Model
	Variant  string `gorm:"type:varchar(64);comment:'这个comment应该被gorm2.0忽略'"`
	Severity string
}

func init() {
	db, _ = gorm.Open(sqlite.Open("file::memory:?cache=shared"))
	db.AutoMigrate(&COVID19{})
}
func TestMemdb(t *testing.T) {
	variant := COVID19{Variant: "BA.5", Severity: "high"}
	err := db.Save(&variant).Error
	if err != nil {
		t.Fatal("saving failed")
	}
	newvariant := COVID19{}
	db.First(&newvariant, 1)
	if newvariant.Variant != "BA.5" {
		t.Fatal("empty db record")
	}
}
