package lms_db

import (
	"testing"

	"github.com/google/uuid"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var db *gorm.DB

func init() {
	db, _ = gorm.Open(sqlite.Open("file::memory:?cache=shared"))
	db.AutoMigrate(&LMSPackageDB{})
}
func TestFind(t *testing.T) {
	pid := uuid.New().String()
	pkg := LMSPackageDB{PackageID: pid}
	db.Create(&pkg)
	null := LMSPackageDB{}
	db.Where("package_id = ?", pid).Find(&null)
	if null.PackageID != pid {
		t.Fatal("find failed")
	}
	null2 := LMSPackageDB{}
	db.Where("package_id = ?", "1234").Find(&null2)
	if null2.PackageID != "" {
		t.Fatal("find failed")
	}
}
