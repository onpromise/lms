package serialnum

import (
	"encoding/json"

	"github.com/zcalusic/sysinfo"
)

type Hardware interface {
	GetHardwareInfos() []string
}
type hardware struct {
	sysinfo.SysInfo
}

func (h hardware) GetHardwareInfos() []string {

	h.GetSysInfo()
	//h.Node.MachineID, h.OS.Architecture, h.Node.Hypervisor
	infos := []string{}
	infos = append(infos, h.Node.MachineID)
	infos = append(infos, h.OS.Architecture)
	infos = append(infos, h.Node.Hypervisor)
	//h.Board.Name, h.Board.Serial, h.Board.Vendor
	infos = append(infos, h.Board.Name)
	infos = append(infos, h.Board.Serial)
	infos = append(infos, h.Board.Vendor)
	bs, _ := json.Marshal(h.CPU)
	infos = append(infos, string(bs))
	bs, _ = json.Marshal(h.Network)
	infos = append(infos, string(bs))
	return infos
}
