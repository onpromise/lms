package package_api

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitee.com/onpromise/lms/consts"
	"gitee.com/onpromise/lms/lib/encrypt"
	"gitee.com/onpromise/lms/models/db/lms_db"
	"gitee.com/onpromise/lms/models/rest/lms_rest"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/lwydyby/logrus"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var apidb *gorm.DB
var engine *gin.Engine

func init() {
	apidb, _ = gorm.Open(sqlite.Open("file::memory:?cache=shared"))
	lms_db.Migrate(apidb)
	apidb.AutoMigrate(&lms_db.LMSPackageDB{})
}
func initContext() gin.HandlerFunc {
	return func(c *gin.Context) {
		alg, err := encrypt.GetDefaultRsaAlg()
		if err != nil {
			alg = nil
			logrus.Error(err)
		}
		c.Set(consts.ENCRYTP_ALG, alg)
		c.Set(consts.CTXDB, apidb)
	}
}
func setup() {
	log.SetFlags(log.Lshortfile)
	engine = gin.Default()
	engine.Use(initContext())
	group := engine.Group("/api/v1/lms")
	Routes(group)
}
func shutdown() {
}
func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}
func buildSubPackages() []lms_rest.SubPackage {
	packages := []lms_rest.SubPackage{}
	pkg := lms_rest.SubPackage{Description: "security component from vendor ONE", ProtectType: "vWAF", PackageId: uuid.NewString(),
		PackageVendorId: "vendor-one",
	}
	packages = append(packages, pkg)
	pkg = lms_rest.SubPackage{Description: "security component from vendor TWO", ProtectType: "vFW", PackageId: uuid.NewString(),
		PackageVendorId: "vendor-two",
	}
	packages = append(packages, pkg)
	return packages
}
func TestPackageDB(t *testing.T) {
	pkg := lms_rest.LMSPackage{}
	pkg.PackageData = ""
	pkg.Description = "this is a test package"
	pkg.PackageId = uuid.NewString()
	pkg.SubPackages = buildSubPackages()
	dbPkg := lms_db.LMSPackageDB{PackageID: pkg.PackageId, Package: lms_db.LMSPackage(pkg)}
	if err := apidb.Create(&dbPkg).Error; err != nil {
		t.Fatal(err)
	}
	readPkg := lms_db.LMSPackageDB{}
	apidb.First(&readPkg)
	if pkg.PackageId != readPkg.Package.PackageId {
		t.Fatal("store failed")
	}
}

//测试一次完整的总包导入
//todo:
// 1. 签名内容补充
func TestPackageAPI(t *testing.T) {
	pkg := lms_rest.LMSPackage{}
	pkg.PackageData = ""
	pkg.Description = "this is a test package"
	pkg.PackageId = uuid.NewString()
	pkg.SubPackages = buildSubPackages()
	alg, _ := encrypt.GetDefaultRsaAlg()
	data := pkg.VerifiedData()
	signature, err := alg.PriEncryptSign(data)
	if err != nil {
		t.Fatal(err)
	}
	pkg.Signature = signature
	bs, _ := json.Marshal(pkg)
	req, err := http.NewRequest("POST", "/api/v1/lms/packages", bytes.NewReader(bs))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}
	recorder := httptest.NewRecorder()
	engine.ServeHTTP(recorder, req)
	if recorder.Result().StatusCode != 200 {
		msg := recorder.Body.Bytes()
		t.Fatal(string(msg))
	}
	pkgs := []lms_db.LMSPackageDB{}
	if err := apidb.Find(&pkgs).Error; err != nil {
		t.Fatal(err)
	}
	//第一个是在上一个测试用例中创建的
	if len(pkgs) != 2 {
		t.Fatal("lmspackage store db failed")
	}
	if pkgs[1].Package.PackageId != pkg.PackageId {
		t.Fatal("test failed")
	}
}
