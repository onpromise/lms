package config

import (
	"fmt"

	"gitee.com/onpromise/lms/consts"
)

type Configs interface {
	GetSignServiceEndpoints(string) (string, string, error)
}
type signConfig struct {
}

var g_config signConfig

func init() {
	g_config = signConfig{}
}

func GetConfig() Configs {
	return &g_config
}
func (c *signConfig) GetSignServiceEndpoints(pkgType string) (string, string, error) {
	token := ""
	switch consts.ProtectType(pkgType) {
	case consts.PROTECT_VWAF:
		return "https:/127.0.0.1:6660/api/v1/packages", token, nil
	case consts.PROTECT_VFW:
		return "https:/127.0.0.1:6661/api/v1/packages", token, nil
	case consts.PROTECT_VSCAN:
		return "https:/127.0.0.1:6662/api/v1/packages", token, nil
	case consts.PROTECT_VBASTION:
		return "https:/127.0.0.1:6663/api/v1/packages", token, nil
	case consts.PROTECT_VLOGAUDIT:
		return "https:/127.0.0.1:6664/api/v1/packages", token, nil
	case consts.PROTECT_VDBAUDIT:
		return "https:/127.0.0.1:6665/api/v1/packages", token, nil
	default:
		return "", "", fmt.Errorf("unkown package type")
	}
}
