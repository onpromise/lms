# 介绍

基于jsonschema定义schema

# schemas
## 风格

### 请求

请求都是发送一个对象,例如
```json
{
  "dummy":{
     "name": "foo"
  }
}
```
如果请求的数据是列表, 发送数据如下:
```json
{
   "dummies":[
     {
       "name": "foo"
     }
   ]
}
```

### 响应

基本结构如下
```json
{
  "code": 200或40xxx,
  "msg": "错误或成功信息",
  "data": {}
}
```
可以扩展分页信息
```json
{
  "code": 200或40xxx,
  "msg": "错误或成功信息",
  "data": {},
  "page": {}
}
```

data部分和请求的风格类似, 如果返回的结果是对象,如下

```json
{
  "code": 200,
  "msg": "错误或成功信息",
  "data": {
    "dummy":{
       "name": "foo"
    }
  }
}
```
如果是列表, 如下
```json
{
  "code": 200,
  "msg": "错误或成功信息",
  "data": {
    "dummies":[
      {
        "name": "foo"
      }
    ]
  }
}
```
可以扩展一些字段, 例如page, data_key
```json
{
  "code": 200,
  "msg": "错误或成功信息",
  "data_key": "dummies",
  "page":{},
  "data": {
    "dummies":[
      {
        "name": "foo"
      }
    ]
  }
}
```
data_key可以辅助client端用某种统一的方式来取出数据

### 错误码

0或200代表正确,40xxx代表自定义的错误编码

## LMS

### 授权汇总信息

### 按组件查询授权信息

查询/创建/更新 授权使用相同的组件授权结构

### 创建授权

主要是分配授权

### 更新授权

主要是延长授权时间

### 撤销授权

取消授权

### 导出授权信息

导出某个组件类型的所有授权信息, 包括到期的和撤销的授权, 授权记录表中记录了授权的历史记录, 并每隔一段时间就要签名，重新确认一遍, 并且所有的历史组成一条链, 防止篡改

### 心跳

组件和signer之间的交互
