package encrypt

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
)

var privKey, pubKey string

func genPrivkeyPem(key *rsa.PrivateKey) string {
	x509PrivateKey := x509.MarshalPKCS1PrivateKey(key)
	privateBlock := pem.Block{
		Type:  privateKeyPrefix,
		Bytes: x509PrivateKey,
	}
	bs := pem.EncodeToMemory(&privateBlock)
	return string(bs)
}
func genPubkeyPem(key rsa.PublicKey) string {
	x509PublicKey, _ := x509.MarshalPKIXPublicKey(&key)
	publicBlock := pem.Block{
		Type:  publicKeyPrefix,
		Bytes: x509PublicKey,
	}
	bs := pem.EncodeToMemory(&publicBlock)
	return string(bs)
}
func init() {
	rsaPrivKey, _ := rsa.GenerateKey(rand.Reader, 2048)
	privKey = genPrivkeyPem(rsaPrivKey)
	pubKey = genPubkeyPem(rsaPrivKey.PublicKey)
}
func GetDefaultRsaAlg() (EncryptAlg, error) {
	return NewRsaEncrypt([]byte(privKey))
}
