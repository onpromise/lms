package encrypt

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"os"
	"strings"
	"testing"
)

var rsaPrivKey *rsa.PrivateKey

//var privKey, pubKey string

//func genPrivkeyPem(key *rsa.PrivateKey) string {
//	x509PrivateKey := x509.MarshalPKCS1PrivateKey(key)
//	privateBlock := pem.Block{
//		Type:  privateKeyPrefix,
//		Bytes: x509PrivateKey,
//	}
//	bs := pem.EncodeToMemory(&privateBlock)
//	return string(bs)
//}
//func genPubkeyPem(key rsa.PublicKey) string {
//	x509PublicKey, _ := x509.MarshalPKIXPublicKey(&key)
//	publicBlock := pem.Block{
//		Type:  publicKeyPrefix,
//		Bytes: x509PublicKey,
//	}
//	bs := pem.EncodeToMemory(&publicBlock)
//	return string(bs)
//}
func setup() {
	rsaPrivKey, _ = rsa.GenerateKey(rand.Reader, 2048)
	privKey = genPrivkeyPem(rsaPrivKey)
	pubKey = genPubkeyPem(rsaPrivKey.PublicKey)
}
func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}
func TestKeyGen(t *testing.T) {
	key, _ := rsa.GenerateKey(rand.Reader, 2048)
	t.Log(fmt.Sprintf("%T", key.PublicKey))
	_privKey := genPrivkeyPem(key)
	_pubKey := genPubkeyPem(key.PublicKey)
	if !strings.Contains(_privKey, privateKeyPrefix) {
		t.Fatal("wrong private key")
	}
	if !strings.Contains(_pubKey, publicKeyPrefix) {
		t.Fatal("wrong public key")
	}
}
func TestRsaKeyParse(t *testing.T) {
	_rsaKey, err := NewRsaKey([]byte(privKey))
	if err != nil {
		t.Fatal(err)
	}
	if !_rsaKey.HasPrivate() || !_rsaKey.HasPublic() {
		t.Fatal("parse wrong")
	}
	t.Log("\n", pubKey)
	if pubKey != string(_rsaKey.GetPubPEM()) {
		t.Fatal("something wrong in key parse/gen")
	}
}
func TestPubEncrypt(t *testing.T) {
	rsaEn, err := NewRsaEncrypt([]byte(pubKey))
	if err != nil {
		t.Fatal(err)
	}
	res, err := rsaEn.PubEncrypt([]byte("hello encrypt!"))
	if err != nil {
		t.Fatal(err)
	}
	rsaDe, err := NewRsaEncrypt([]byte(privKey))
	if err != nil {
		t.Fatal(err)
	}
	des, err := rsaDe.PriDecrypt(res)
	if err != nil {
		t.Fatal(err)
	}
	if string(des) != "hello encrypt!" {
		t.Fatal("decrypt test failed")
	}
}

func TestPrivEncrypt(t *testing.T) {
	rsaEn, err := NewRsaEncrypt([]byte(privKey))
	if err != nil {
		t.Fatal(err)
	}
	res, err := rsaEn.PriEncrypt([]byte("hello encrypt!"))
	if err != nil {
		t.Fatal(err)
	}
	rsaDe, err := NewRsaEncrypt([]byte(pubKey))
	if err != nil {
		t.Fatal(err)
	}
	des, err := rsaDe.PubDecrypt(res)
	if err != nil {
		t.Fatal(err)
	}
	if string(des) != "hello encrypt!" {
		t.Log(string(des))
		t.Fatal("decrypt test failed")
	}
}
func TestSign(t *testing.T) {
	rsaEn, err := NewRsaEncrypt([]byte(privKey))
	if err != nil {
		t.Fatal(err)
	}
	val, err := rsaEn.PriEncryptSign([]byte("hello signer"))
	if err != nil {
		t.Fatal(err)
	}
	t.Log(val)
}
func TestVerify(t *testing.T) {
	rsaEn, err := NewRsaEncrypt([]byte(privKey))
	if err != nil {
		t.Fatal(err)
	}
	val, err := rsaEn.PriEncryptSign([]byte("hello signer"))
	if err != nil {
		t.Fatal(err)
	}
	err = rsaEn.VerifySign([]byte("hello signer"), val)
	if err != nil {
		t.Fatal("verify failed")
	}
}
