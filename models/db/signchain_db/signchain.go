package signchain_db

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"gitee.com/onpromise/lms/consts"
	"gitee.com/onpromise/lms/lib/encrypt"
	"github.com/google/uuid"
	"github.com/lwydyby/logrus"
	"gorm.io/gorm"
)

type SignOP string
type SignType string

const (
	FOUNDATION_CREATE     SignOP   = "foundation_create"
	PACKAGE_IMPORT        SignOP   = "package_import"
	LICENSE_CREATE        SignOP   = "license_create"
	LICENSE_APPEND        SignOP   = "license_append"
	LICENSE_REVERT        SignOP   = "license_revert"
	LICENSE_OVERWRITE     SignOP   = "license_overwrite"
	LICENSE_WATCH_DOG     SignOP   = "license_watchdog"
	LICENSE_MANAGE_TYPE   SignType = "license_manage_type"
	LICENSE_GUARD_TYPE    SignType = "license_guard_type"
	BLOCK_FOUNDATION_TYPE SignType = "block_foundation_type"
)

type ParentBlock struct {
	SignedAt time.Time `json:"signed_at"`
	UUID     string    `json:"uuid"`
	SignType SignType  `json:"sign_type"`
	SignOP   SignOP    `json:"sign_op"`
}
type SignContent struct {
	ServiceBornedAt  time.Time `json:"service_borned_at"`  //服务授权导入时间
	ServiceExpiredAt time.Time `json:"service_expired_at"` //服务授权的过期时间
	Extra            string    `json:"extra"`              //一些额外信息序列化后存到这个字段
}
type SignChainBlock struct {
	gorm.Model
	BlockName      string      `json:"block_name"`
	SignedAt       time.Time   `json:"signed_at"`                     //签名时间
	UUID           string      `gorm:"unique" json:"uuid"`            //签名的唯一uuid
	Parent         ParentBlock `gorm:"type:JSON" json:"parent"`       //父Block信息
	SignType       SignType    `json:"sign_type"`                     //签名的类型
	Content        SignContent `gorm:"type:JSON" json:"sign_content"` //签名内容,其它非共性的, 都放到这个字段下面
	SignOP         SignOP      `json:"sign_op"`                       //签名行为
	SignTotalCnt   int         `json:"sign_total"`                    //当前所有授权的数量
	SignTotalMonth int         `json:"sign_total_month"`              //当前所有授权的总时间, 以月为单位
	UsedTotalMonth int         `json:"used_total_month"`              //已经使用的所有授权的总时间, 以月为单位
	Signature      string      `json:"signature"`                     //签名
	LMSRequestSign string      `json:"lmsrequest_sign"`               //lms服务向signer申请授权的签名
}
type SignChain struct {
	Max     int              `json:"max"`      //可追溯的最大数
	BeginAt time.Time        `json:"begin_at"` //可追溯到的时间
	Blocks  []SignChainBlock `json:"blocks"`   //SignChainBlock
}

func (m *ParentBlock) Scan(value interface{}) error {
	switch data := value.(type) {
	case string:
		return json.Unmarshal([]byte(data), m)
	}
	return json.Unmarshal(value.([]byte), m)
}
func (m ParentBlock) Value() (driver.Value, error) {
	return json.Marshal(m)
}
func (m *SignContent) Scan(value interface{}) error {
	switch data := value.(type) {
	case string:
		return json.Unmarshal([]byte(data), m)
	}
	return json.Unmarshal(value.([]byte), m)
}
func (m SignContent) Value() (driver.Value, error) {
	return json.Marshal(m)
}
func (m *SignOP) Scan(value interface{}) error {
	switch data := value.(type) {
	case string:
		return json.Unmarshal([]byte(data), m)
	}
	return json.Unmarshal(value.([]byte), m)
}
func (m SignOP) Value() (driver.Value, error) {
	return json.Marshal(m)
}
func (m *SignType) Scan(value interface{}) error {
	switch data := value.(type) {
	case string:
		return json.Unmarshal([]byte(data), m)
	}
	return json.Unmarshal(value.([]byte), m)
}
func (m SignType) Value() (driver.Value, error) {
	return json.Marshal(m)
}
func (b SignChainBlock) TableName() string {
	return "sign_chain_blocks"
}
func (b *SignChainBlock) signContent() []byte {
	signMap := map[string]interface{}{}
	blockMap := map[string]interface{}{}
	bs, _ := json.Marshal(*b)
	json.Unmarshal(bs, &blockMap)
	keys := []string{"signed_at", "uuid", "parent", "sign_type", "sign_content", "sign_op", "sign_total", "sign_total_month", "lmsrequest_sign"}
	for _, key := range keys {
		if val, ok := blockMap[key]; ok {
			signMap[key] = val
		} else {
			logrus.Fatal(fmt.Errorf("key %s missed", key))
		}
	}
	bs, _ = json.Marshal(signMap)
	return bs
}
func (b *SignChainBlock) VerifyParent(parent SignChainBlock) error {
	if b.Parent.UUID != parent.UUID {
		return fmt.Errorf("wrong parent uuid:%s", parent.UUID)
	}
	if b.Parent.SignedAt != parent.SignedAt {
		return fmt.Errorf("wrong parent signed_at:%v", parent.SignedAt)
	}
	if b.Parent.SignOP != parent.SignOP {
		return fmt.Errorf("wrong parent sign op:%v", parent.SignOP)
	}
	if b.Parent.SignType != parent.SignType {
		return fmt.Errorf("wrong parent sign type:%v", parent.SignType)
	}
	return nil
}

func (b *SignChainBlock) Sign(alg encrypt.EncryptAlg) error {
	content := b.signContent()
	val, err := alg.PriEncryptSign(content)
	if err != nil {
		logrus.Error(err)
		return err
	}
	b.Signature = val
	return nil
}
func (b *SignChainBlock) Verify(alg encrypt.EncryptAlg) error {
	content := b.signContent()
	err := alg.VerifySign(content, b.Signature)
	if err != nil {
		logrus.Error(err)
	}
	return nil
}
func (c *SignChain) Load(tx *gorm.DB) error {
	blocks := []SignChainBlock{}
	tx = tx.Order("signed_at desc")
	if c.Max > 0 {
		tx = tx.Limit(c.Max)
	}
	if !c.BeginAt.IsZero() {
		tx = tx.Where("signed_at >= ?", c.BeginAt)
	}
	err := tx.Find(&blocks).Error
	if err != nil {
		log.Println("error:", err)
		return err
	}
	c.Blocks = blocks
	return nil
}
func (c *SignChain) Verify(alg encrypt.EncryptAlg) error {
	if len(c.Blocks) == 0 {
		return nil
	}
	prev := c.Blocks[0]
	if err := prev.Verify(alg); err != nil {
		logrus.Error(err)
		return err
	}
	for _, block := range c.Blocks[1:] {
		if err := prev.VerifyParent(block); err != nil {
			logrus.Error(err)
			return err
		}
		if err := block.Verify(alg); err != nil {
			logrus.Error(err)
			return err
		}
		prev = block
	}
	return nil
}
func (c *SignChain) AppendPackageBlock(deadLine time.Time, extra string, db *gorm.DB, alg encrypt.EncryptAlg) error {
	parentBlock := c.Blocks[0]
	now := time.Now().UTC()
	uuid := uuid.New().String()
	parent := ParentBlock{}
	parent.SignedAt = parentBlock.SignedAt
	parent.UUID = parentBlock.UUID
	parent.SignOP = parentBlock.SignOP
	parent.SignType = parentBlock.SignType
	content := SignContent{}
	content.ServiceBornedAt = now
	content.ServiceExpiredAt = deadLine
	content.Extra = extra
	chainBlock := SignChainBlock{SignedAt: now, SignOP: PACKAGE_IMPORT, UUID: uuid, Parent: parent, Content: content}
	if err := chainBlock.Sign(alg); err != nil {
		logrus.Error(err)
		return err
	}
	if err := db.Create(&chainBlock).Error; err != nil {
		logrus.Error(err)
		return err
	}
	return nil

}
func FoundationBlock(alg encrypt.EncryptAlg) SignChainBlock {
	chainBlock := SignChainBlock{SignedAt: time.Now().UTC(), SignOP: FOUNDATION_CREATE, UUID: uuid.NewString(), BlockName: consts.FOUNDATION_BLOCK}
	chainBlock.Parent = ParentBlock{SignType: BLOCK_FOUNDATION_TYPE}
	chainBlock.Sign(alg)
	return chainBlock
}
