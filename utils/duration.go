package utils

import (
	"fmt"
	"regexp"
	"strconv"
	"time"
)

func ParseDuration(d string) (time.Duration, error) {
	duration := 0 * time.Second
	if d == "" {
		return duration, nil
	}
	pat := regexp.MustCompile(`(\d+)(d|m)`)
	matches := pat.FindAllStringSubmatch(d, -1)
	if len(matches) <= 0 || len(matches[0]) < 3 {
		return duration, fmt.Errorf("wrong duration format")
	}
	now := time.Now().UTC()
	end := now
	switch matches[0][2] {
	case "d":
		{
			days, err := strconv.Atoi(matches[0][1])
			if err != nil {
				return duration, err
			}
			end = now.AddDate(0, 0, days)
		}
	case "m":
		{
			months, err := strconv.Atoi(matches[0][1])
			if err != nil {
				return duration, err
			}
			end = now.AddDate(0, months, 0)
		}
	default:
		return duration, fmt.Errorf("wrong duration format, just suport <n>d <n>m")

	}
	duration = end.Sub(now)
	return duration, nil
}
func ParseAppendDuration(d string) (time.Duration, error) {
	if d == "" {
		return ParseDuration(d)
	}
	if d[0] != '+' {
		return 0 * time.Second, fmt.Errorf("not append duration")
	}
	return ParseDuration(d[1:])
}
func ParseMonthDay(d string) (days, months int, err error) {
	if d == "" {
		return
	}
	pat := regexp.MustCompile(`(\d+)(d|m)`)
	matches := pat.FindAllStringSubmatch(d, -1)
	if len(matches) <= 0 || len(matches[0]) < 3 {
		err = fmt.Errorf("wrong duration format")
		return
	}
	switch matches[0][2] {
	case "d":
		{
			days, err = strconv.Atoi(matches[0][1])
		}
	case "m":
		{
			months, err = strconv.Atoi(matches[0][1])
		}
	default:
		err = fmt.Errorf("wrong duration format, just suport <n>d <n>m")
	}
	return
}
