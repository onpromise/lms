package utils

import "testing"

func TestDurationParse(t *testing.T) {
	duration, err := ParseDuration("12m")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(duration.Hours())
	if duration.Hours() < 8000 {
		t.Fatal("wrong parse")
	}
	duration, err = ParseDuration("31day")
	if err != nil {
		t.Fatal(err)
	}
	if duration.Hours() != 744 {
		t.Fatal("wrong parse")
	}
}
func TestAppendDuration(t *testing.T) {
	duration, err := ParseAppendDuration("+1d")
	if err != nil {
		t.Fatal(err)
	}
	if duration.Hours() != 24 {
		t.Fatal("wrong append duration format")
	}
	_, err = ParseAppendDuration("1d")
	if err == nil {
		t.Fatal("wront append duration format")
	}
}
