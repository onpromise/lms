package lms_db

import (
	"database/sql/driver"
	"encoding/json"

	"gorm.io/gorm"
)

type MonthSign struct {
	// 签名内容
	Sign string `json:"sign,omitempty" yaml:"sign,omitempty"`

	// 签名时间
	SignedAt string `json:"signed_at,omitempty" yaml:"signed_at,omitempty"`
}
type License struct {
	gorm.Model
	// 授权分配时间
	AssignedAt string `json:"assigned_at,omitempty" yaml:"assigned_at,omitempty"`

	// 组件id
	ComponentId string `json:"component_id,omitempty" yaml:"component_id,omitempty"`

	// 组件ip
	ComponentIp string `json:"component_ip,omitempty" yaml:"component_ip,omitempty"`

	// 组件名称
	ComponentName string `json:"component_name,omitempty" yaml:"component_name,omitempty"`

	// 组件类型,Firewall,Waf,DBAudit,...
	ComponentType string `json:"component_type,omitempty" yaml:"component_type,omitempty"`

	// 授权到期时间
	ExpiredAt string `json:"expired_at,omitempty" yaml:"expired_at,omitempty"`

	// 授权对应的唯一uuid
	LicenseId string `json:"license_id,omitempty" yaml:"license_id,omitempty"`

	// MonthSigns corresponds to the JSON schema field "month_signs".
	MonthSigns []MonthSign `gorm:"type:JSON" json:"month_signs,omitempty" yaml:"month_signs,omitempty"`

	// 授权分配者
	Signer string `json:"signer,omitempty" yaml:"signer,omitempty"`

	// 规格
	Spec string `json:"spec,omitempty" yaml:"spec,omitempty"`
}

func (m *MonthSign) Scan(value interface{}) error {
	switch data := value.(type) {
	case string:
		return json.Unmarshal([]byte(data), m)
	}
	return json.Unmarshal(value.([]byte), m)
}
func (m MonthSign) Value() (driver.Value, error) {
	return json.Marshal(m)
}
