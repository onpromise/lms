package serialnum

import (
	"strings"

	"gitee.com/onpromise/lms/lib/encrypt"
)

type serialNum string
type SerialNum interface {
	String() string                              //获取序列号串
	Generate(encrypt.EncryptAlg, Hardware) error //创建序列号,value为设备信息序列化后的值,也可以是一个加盐效果的值
	Count() int                                  //有多少序列号
	Slice() []string                             //序列号列表
	Append(string)                               //追加一个序列号
}

func (sn *serialNum) String() string {
	return string(*sn)
}

//按顺序合并成一个串, 然后加密,加盐, hash
func (sn *serialNum) Generate(alg encrypt.EncryptAlg, hw Hardware) error {
	infos := hw.GetHardwareInfos()
	value := strings.Join(infos, ",")
	serial, err := alg.PriEncryptSign([]byte(value))
	if err != nil {
		return err
	}
	*sn = serialNum(serial)
	return nil
}
func (sn *serialNum) Slice() []string {
	if *sn == "" {
		return []string{}
	}
	ss := strings.Split(string(*sn), ",")
	return ss
}
func (sn *serialNum) Count() int {
	return len(sn.Slice())
}
func (sn *serialNum) serialMap() map[string]interface{} {
	m := map[string]interface{}{}
	ss := strings.Split(string(*sn), ",")
	for _, num := range ss {
		m[num] = struct{}{}
	}
	return m
}
func (sn *serialNum) Append(newsn string) {
	if _, ok := sn.serialMap()[newsn]; !ok {
		ss := sn.Slice()
		ss = append(ss, newsn)
		*sn = serialNum(strings.Join(ss, ","))
	}
}
func NewSerialNum() SerialNum {
	sn := new(serialNum)
	return sn
}
