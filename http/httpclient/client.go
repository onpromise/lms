package httpclient

import (
	"io"
	"io/ioutil"
	"net/http"
)

func Post(url string, body io.Reader, headers map[string]string) (int, []byte, error) {
	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		return 0, nil, err
	}
	for key, val := range headers {
		req.Header.Set(key, val)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return 0, nil, err
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	return resp.StatusCode, data, err
}
