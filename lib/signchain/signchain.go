package signchain

import (
	"context"
	"fmt"
	"strings"
	"sync"
	"time"

	"gitee.com/onpromise/lms/common"
	"gitee.com/onpromise/lms/lib/encrypt"
	"gitee.com/onpromise/lms/models/db/signchain_db"
	"github.com/lwydyby/logrus"
	"gorm.io/gorm"
)

type ValidFoundationTime interface {
	ValidFoundationBlockTime(db *gorm.DB, createAt time.Time) bool
}
type SignChainService interface {
	CheckFoundationBlock(ValidFoundationTime) error //检测第一个块, 如果没有就默认创建
	AppendPackageBlock(time.Time, string) error
	AppendLicenseBlock() error
}
type signChainService struct {
	ctx  context.Context
	lock sync.Mutex
}

func CreateSignChainService(ctx context.Context) SignChainService {
	chain := signChainService{ctx: ctx, lock: sync.Mutex{}}
	return &chain
}
func (s *signChainService) getDBAlgs() (*gorm.DB, encrypt.EncryptAlg, error) {
	db, err := common.GetCtxDB(s.ctx)
	if err != nil {
		logrus.Error(err)
		return nil, nil, err
	}
	alg, err := common.GetEncryptAlg(s.ctx)
	if err != nil {
		logrus.Error(err)
		return nil, nil, err
	}
	return db, alg, nil
}
func (s *signChainService) CheckFoundationBlock(valid ValidFoundationTime) error {
	blocks := []signchain_db.SignChainBlock{}
	db, alg, err := s.getDBAlgs()
	if err != nil {
		logrus.Error(err)
		return err
	}
	if err := db.Find(&blocks).Error; err != nil {
		logrus.Error(err)
		return err
	}
	if len(blocks) >= 2 {
		err = fmt.Errorf("there should be just one foundation block")
		return err
	}
	if len(blocks) <= 0 {
		foundation := signchain_db.FoundationBlock(alg)
		err := db.Create(&foundation).Error
		if err != nil {
			logrus.Error(err)
			return err
		}
	}
	if err = blocks[0].Verify(alg); err != nil {
		logrus.Error(err)
		return err
	}
	if !valid.ValidFoundationBlockTime(db, blocks[0].SignedAt) {
		return fmt.Errorf("foundation block created after imported")
	}
	return nil
}

//还缺少子包入链的行为
func (s *signChainService) AppendPackageBlock(deadLine time.Time, content string) error {
	locked := false
	db, alg, err := s.getDBAlgs()
	if err != nil {
		logrus.Error(err)
		return err
	}
	block := signchain_db.SignChainBlock{}
	err = db.Exec(fmt.Sprintf("LOCK TABLE %s write", block.TableName())).Error
	for {
		if err != nil {
			if strings.Contains(err.Error(), `near "LOCK": syntax error`) {
				//error in sqlite3, ignore
				break
			}
			logrus.Error(err)
			return err
		}
		locked = true
		break
	}
	defer func() {
		if locked {
			err := db.Exec("UNLOCK TABLES").Error
			if err != nil {
				logrus.Fatal(err)
			}
		}
	}()
	chains := signchain_db.SignChain{Max: 10}
	err = chains.Load(db)
	if err != nil {
		logrus.Error(err)
		return err
	}
	err = chains.Verify(alg)
	if err != nil {
		logrus.Error(err)
		return err
	}
	if err := chains.AppendPackageBlock(deadLine, content, db, alg); err != nil {
		logrus.Error(err)
		return err
	}
	return nil
}
func (s *signChainService) AppendLicenseBlock() error {
	return nil
}
