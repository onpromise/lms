// Code generated by github.com/atombender/go-jsonschema, DO NOT EDIT.

package lms_rest

type ApplyRequest struct {
	// 组件ID
	ComponentId string `json:"component_id,omitempty" yaml:"component_id,omitempty"`

	// 组件ip
	ComponentIp string `json:"component_ip,omitempty" yaml:"component_ip,omitempty"`

	// 组件Mac地址
	ComponentMac string `json:"component_mac,omitempty" yaml:"component_mac,omitempty"`

	// 组件名称
	ComponentName string `json:"component_name,omitempty" yaml:"component_name,omitempty"`

	// 组件类型,Firewall,Waf,DBAudit,...
	ComponentType string `json:"component_type,omitempty" yaml:"component_type,omitempty"`

	// 申请的时长,以月为单位, 例如如果申请一年, duration就是12m
	Duration string `json:"duration,omitempty" yaml:"duration,omitempty"`

	// 规格
	Spec string `json:"spec,omitempty" yaml:"spec,omitempty"`
}

// 组件授权信息
type ComponentLicense struct {
	// Code corresponds to the JSON schema field "code".
	Code int `json:"code,omitempty" yaml:"code,omitempty"`

	// Data corresponds to the JSON schema field "data".
	Data ComponentLicenseData `json:"data,omitempty" yaml:"data,omitempty"`

	// Msg corresponds to the JSON schema field "msg".
	Msg string `json:"msg,omitempty" yaml:"msg,omitempty"`
}

type ComponentLicenseData struct {
	// LicenseInfo corresponds to the JSON schema field "license_info".
	LicenseInfo License `json:"license_info,omitempty" yaml:"license_info,omitempty"`
}

type License struct {
	// 授权分配时间
	AssignedAt string `json:"assigned_at,omitempty" yaml:"assigned_at,omitempty"`

	// 组件id
	ComponentId string `json:"component_id,omitempty" yaml:"component_id,omitempty"`

	// 组件ip
	ComponentIp string `json:"component_ip,omitempty" yaml:"component_ip,omitempty"`

	// 组件名称
	ComponentName string `json:"component_name,omitempty" yaml:"component_name,omitempty"`

	// 组件类型,Firewall,Waf,DBAudit,...
	ComponentType string `json:"component_type,omitempty" yaml:"component_type,omitempty"`

	// 授权到期时间
	ExpiredAt string `json:"expired_at,omitempty" yaml:"expired_at,omitempty"`

	// 授权对应的唯一uuid
	LicenseId string `json:"license_id,omitempty" yaml:"license_id,omitempty"`

	// MonthSigns corresponds to the JSON schema field "month_signs".
	MonthSigns []MonthSign `json:"month_signs,omitempty" yaml:"month_signs,omitempty"`

	// 授权分配者
	Signer string `json:"signer,omitempty" yaml:"signer,omitempty"`

	// 规格
	Spec string `json:"spec,omitempty" yaml:"spec,omitempty"`
}

// 每月签名一次
type MonthSign struct {
	// 签名内容
	Sign string `json:"sign,omitempty" yaml:"sign,omitempty"`

	// 签名时间
	SignedAt string `json:"signed_at,omitempty" yaml:"signed_at,omitempty"`
}
