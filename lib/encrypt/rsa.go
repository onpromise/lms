package encrypt

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"

	//"log"

	"strings"

	"github.com/sirupsen/logrus"

	"github.com/wenzhenxi/gorsa"
)

const (
	privateKeyPrefix = "RSA PRIVATE KEY "
	publicKeyPrefix  = "RSA PUBLIC KEY "
)

type rsaKey struct {
	rawKey  []byte
	pubPem  []byte
	privKey *rsa.PrivateKey
	pubKey  *rsa.PublicKey
}
type rsaEncrypt struct {
	key EncryptKey
}

func (key *rsaKey) Parse() error {
	if len(key.rawKey) <= 0 {
		return fmt.Errorf("empty key bytes")
	}
	getPub := func() {
		x509PublicKey, err := x509.MarshalPKIXPublicKey(&key.privKey.PublicKey)
		if err != nil {
			logrus.Error(err)
		}
		publicBlock := pem.Block{
			Type:  publicKeyPrefix,
			Bytes: x509PublicKey,
		}
		bs := pem.EncodeToMemory(&publicBlock)
		key.pubPem = bs
		pubpkcs1, err := x509.ParsePKCS1PublicKey(x509PublicKey)
		if err == nil {
			key.pubKey = pubpkcs1
			return
		}
		pub, err := x509.ParsePKIXPublicKey(x509PublicKey)
		if err == nil {
			key.pubKey = pub.(*rsa.PublicKey)
			return
		}
	}
	if bytes.Contains(key.rawKey, []byte(privateKeyPrefix)) {
		//私钥解析
		block, _ := pem.Decode(key.rawKey)
		if block == nil {
			return fmt.Errorf("get private key failed")
		}
		pri, err := x509.ParsePKCS1PrivateKey(block.Bytes)
		if err == nil {
			key.privKey = pri
			getPub()
			return nil
		}
		pri8, err := x509.ParsePKCS8PrivateKey(block.Bytes)
		if err != nil {
			return err
		}
		key.privKey = pri8.(*rsa.PrivateKey)
		getPub()
		return nil
	}
	//公钥解析
	block, _ := pem.Decode(key.rawKey)
	if block == nil {
		return fmt.Errorf("get public key failed")
	}
	pubpkcs1, err := x509.ParsePKCS1PublicKey(block.Bytes)
	if err == nil {
		key.pubKey = pubpkcs1
		return nil
	}
	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err == nil {
		key.pubKey = pub.(*rsa.PublicKey)
		key.pubPem = key.rawKey
		return nil
	}

	return err
}
func (key *rsaKey) HasPublic() bool {
	if key.pubKey != nil || key.privKey != nil {
		return true
	}
	return false
}
func (key *rsaKey) HasPrivate() bool {
	return key.privKey != nil
}
func (key *rsaKey) GetPrivPEM() string {
	return string(key.rawKey)
}
func (key *rsaKey) GetPubPEM() string {
	return string(key.pubPem)
}
func (key *rsaKey) GetPrivKey() interface{} {
	return key.privKey
}
func (key *rsaKey) GetPubKey() interface{} {
	return key.pubKey
}
func (enc *rsaEncrypt) PubEncrypt(data []byte) (string, error) {
	pubKey := enc.key.GetPubKey().(*rsa.PublicKey)
	encryptedBytes, err := rsa.EncryptOAEP(
		sha256.New(),
		rand.Reader,
		pubKey,
		data,
		nil)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(encryptedBytes), nil
}
func (enc *rsaEncrypt) PriEncrypt(data []byte) (string, error) {
	privKey := enc.key.GetPrivPEM()
	encryptedBytes, err := gorsa.PriKeyEncrypt(string(data), privKey)
	if err != nil {
		return "", err
	}

	if err != nil {
		return "", err
	}
	return encryptedBytes, nil
}
func (enc *rsaEncrypt) PriDecrypt(data string) ([]byte, error) {
	bs, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		return []byte{}, err
	}
	privKey := enc.key.GetPrivKey().(*rsa.PrivateKey)
	decryptedBytes, err := privKey.Decrypt(nil, bs, &rsa.OAEPOptions{Hash: crypto.SHA256})
	return decryptedBytes, nil
}
func (enc *rsaEncrypt) PubDecrypt(data string) ([]byte, error) {
	bs, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		return []byte{}, err
	}
	pubKey := enc.key.GetPubPEM()
	grsa := gorsa.RSASecurity{}
	if err := grsa.SetPublicKey(pubKey); err != nil {
		return []byte{}, err
	}

	rsadata, err := grsa.PubKeyDECRYPT(bs)
	if err != nil {
		return []byte{}, err
	}
	return rsadata, nil
}
func (enc *rsaEncrypt) PriEncryptSign(data []byte) (string, error) {
	//先不加密, 直接hash后签名
	hash := sha256.New()
	_, err := hash.Write(data)
	if err != nil {
		return "", err
	}
	hashSum := hash.Sum(nil)
	privKey := enc.key.GetPrivKey().(*rsa.PrivateKey)
	signature, err := rsa.SignPSS(rand.Reader, privKey, crypto.SHA256, hashSum, nil)
	if err != nil {
		return "", err
	}
	encode := func(bs []byte) string {
		return base64.StdEncoding.EncodeToString(bs)

	}
	val := fmt.Sprintf("%s.%s", encode(hashSum), encode(signature))
	return val, nil
}
func (enc *rsaEncrypt) VerifySign(data []byte, signature string) error {
	hash := sha256.New()
	_, err := hash.Write(data)
	if err != nil {
		logrus.Error(err)
		return err
	}
	hashSum := hash.Sum(nil)
	if strings.Contains(signature, ".") {
		ss := strings.Split(signature, ".")
		signature = ss[1]
	}

	signbytes, _ := base64.StdEncoding.DecodeString(signature)
	pubKey := enc.key.GetPubKey().(*rsa.PublicKey)
	if pubKey == nil {
		logrus.Warn("pbulic key is nil")
	}
	err = rsa.VerifyPSS(pubKey, crypto.SHA256, hashSum, signbytes, nil)
	return err
}
func NewRsaKey(pemBytes []byte) (EncryptKey, error) {
	key := new(rsaKey)
	key.rawKey = pemBytes
	if err := key.Parse(); err != nil {
		return nil, err
	}
	return key, nil
}
func NewRsaEncrypt(pemBytes []byte) (EncryptAlg, error) {
	enc := new(rsaEncrypt)
	_rsaKey, err := NewRsaKey(pemBytes)
	if err != nil {
		return nil, err
	}
	enc.key = _rsaKey
	return enc, nil
}
