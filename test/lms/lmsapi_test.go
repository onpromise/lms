//测试授权相关的API
//1. 授权信息查询
//2. 分配授权
//3. 撤销授权
//4. 更新授权
//
package lms

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"gitee.com/onpromise/lms/api/lms_api"
	"gitee.com/onpromise/lms/consts"
	"gitee.com/onpromise/lms/models/db/lms_db"
	"gitee.com/onpromise/lms/models/rest/lms_rest"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var apidb *gorm.DB
var engine *gin.Engine

type TestTable struct {
	gorm.Model
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func init() {
	apidb, _ = gorm.Open(sqlite.Open("file::memory:?cache=shared"))
	apidb.AutoMigrate(&TestTable{})
	lms_db.Migrate(apidb)
}
func setup() {
	log.SetFlags(log.Lshortfile)
	engine = gin.Default()
	engine.Use(initContext())
	group := engine.Group("/api/v1/lms")
	lms_api.Routes(group)
}
func shutdown() {
}
func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}
func initContext() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set(consts.CTXDB, apidb)
	}
}
func TestContextInit(t *testing.T) {
	r := gin.Default()
	data := TestTable{Name: "nobody", Age: 200}
	apidb.Create(&data)
	r.Use(initContext())
	r.GET("/testdb", func(c *gin.Context) {
		nobody := TestTable{}

		ctxdb := c.Value(consts.CTXDB).(*gorm.DB)
		if ctxdb != nil {
			ctxdb.First(&nobody)
			c.JSON(http.StatusOK, gin.H{"data": nobody})
		} else {
			c.JSON(http.StatusOK, gin.H{"data": nil})
		}
	})
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/testdb", nil)
	r.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Fatal("auth test failed")
	}
	bs, _ := ioutil.ReadAll(recorder.Result().Body)
	m := map[string]TestTable{}
	err := json.Unmarshal(bs, &m)
	if err != nil {
		t.Fatal(err)
	}
	if m["data"].Age != 200 {
		t.Fatal("i am 200 years old")
	}
}

func TestLmsSummaryQuery(t *testing.T) {
	licCount := lms_db.LicenseCount{
		Assigned:  10,
		Available: 20,
		Failed:    0,
		Total:     30,
	}
	apidb.Create(&licCount)

	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/lms/summary", nil)
	req.Header.Set("Content-Type", "application/json")
	engine.ServeHTTP(recorder, req)
	bs, _ := ioutil.ReadAll(recorder.Result().Body)
	//t.Log(string(bs))
	if recorder.Code != http.StatusOK {
		t.Fatal("test /api/v1/lms/summary failed")
	}
	resp := lms_rest.SummaryResp{}
	json.Unmarshal(bs, &resp)
	if resp.Data.Summary.CountsBySigner[0].Total != 30 {
		t.Fatal("summary api test failed")
	}
}
func TestApplyLicense(t *testing.T) {
	recorder := httptest.NewRecorder()
	request := lms_rest.ApplyRequest{}
	request.ComponentId = "1234-1234"
	request.ComponentIp = "192.168.8.8"
	request.ComponentMac = "demo"
	request.Duration = "1m"
	bs, _ := json.Marshal(request)
	//请求创建新的授权
	req, _ := http.NewRequest("POST", "/api/v1/lms/licenses", bytes.NewReader(bs))
	req.Header.Set("Content-Type", "application/json")
	engine.ServeHTTP(recorder, req)
	bs, _ = ioutil.ReadAll(recorder.Result().Body)
	t.Log(string(bs))
	if recorder.Result().StatusCode != http.StatusOK {
		t.Fatal("apply license failed")
	}
}
func TestRevertLicense(t *testing.T) {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/lms/licenses", nil)
	req.Header.Set("Content-Type", "application/json")
	engine.ServeHTTP(recorder, req)
	bs, _ := ioutil.ReadAll(recorder.Result().Body)
	t.Log(string(bs))
	if recorder.Result().StatusCode != http.StatusOK {
		t.Fatal("apply license failed")
	}
	data := struct {
		Data map[string][]lms_db.License `json:"data"`
	}{}
	err := json.Unmarshal(bs, &data)
	if err != nil {
		t.Fatal(err)
	}
	if len(data.Data["licenses"]) != 1 {
		t.Fatal("unmarshal error")
	}
	if "" == data.Data["licenses"][0].ComponentIp {
		t.Fatal("unmarshal error")
	}
	//暂定删除授权就是撤销授权
	license_id := data.Data["licenses"][0].LicenseId
	recorder = httptest.NewRecorder()
	req, _ = http.NewRequest("DELETE", fmt.Sprintf("/api/v1/lms/licenses/%s", license_id), nil)
	engine.ServeHTTP(recorder, req)
	if recorder.Result().StatusCode != http.StatusOK {
		t.Fatal("revert license failed")
	}
	bs, _ = ioutil.ReadAll(recorder.Result().Body)
	t.Log(string(bs))
}

//目前只是想到支持授权的延期和规格的更新(相当于扩容), 这也是主要的操作
//先添加一个授权, 然后再更新
func TestUpdateLicense(t *testing.T) {
	now := time.Now().UTC()
	recorder := httptest.NewRecorder()
	request := lms_rest.ApplyRequest{}
	request.ComponentId = uuid.New().String()
	request.ComponentIp = "192.168.8.8"
	request.ComponentMac = "demo"
	request.Spec = "standard"
	request.Duration = "1m"
	bs, _ := json.Marshal(request)
	//请求创建新的授权
	req, _ := http.NewRequest("POST", "/api/v1/lms/licenses", bytes.NewReader(bs))
	req.Header.Set("Content-Type", "application/json")
	engine.ServeHTTP(recorder, req)
	if recorder.Result().StatusCode != http.StatusOK {
		t.Fatal("create new license failed")
	}
	data := struct {
		Data gin.H `json:"data"`
	}{}
	bs, _ = ioutil.ReadAll(recorder.Result().Body)
	json.Unmarshal(bs, &data)
	licenseid := data.Data["license_id"].(string)
	t.Log(licenseid)
	request.Spec = "advanced"
	request.Duration = "+1m"
	bs, _ = json.Marshal(request)
	req, _ = http.NewRequest("PUT", fmt.Sprintf("/api/v1/lms/licenses/%s", licenseid), bytes.NewReader(bs))
	req.Header.Set("Content-Type", "application/json")
	recorder = httptest.NewRecorder()
	engine.ServeHTTP(recorder, req)
	bs, _ = ioutil.ReadAll(recorder.Result().Body)
	if recorder.Result().StatusCode != http.StatusOK {
		t.Log(string(bs))
		t.Fatal("update license failed")
	}
	req, _ = http.NewRequest("GET", fmt.Sprintf("/api/v1/lms/licenses/%s", licenseid), nil)
	req.Header.Set("Content-Type", "application/json")
	recorder = httptest.NewRecorder()
	engine.ServeHTTP(recorder, req)
	bs, _ = ioutil.ReadAll(recorder.Result().Body)
	if recorder.Result().StatusCode != http.StatusOK {
		t.Fatal("get license failed")
	}
	license_data := struct {
		Data lms_rest.License `json:"data"`
	}{}
	json.Unmarshal(bs, &license_data)
	if license_data.Data.Spec != "advanced" {
		t.Log(string(bs))
		t.Fatal("updated failed")
	}
	expired_at, _ := time.Parse(time.RFC3339, license_data.Data.ExpiredAt)
	if expired_at.Sub(now).Hours() < 59*24 {
		t.Fatal("updated failed")
	}
}
