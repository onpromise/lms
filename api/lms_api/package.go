package lms_api

import (
	"net/http"

	"gitee.com/onpromise/lms/common"
	"gitee.com/onpromise/lms/lib/encrypt"
	"gitee.com/onpromise/lms/lms_packages"
	"gitee.com/onpromise/lms/models/rest/lms_rest"
	"github.com/gin-gonic/gin"
)

func importPackage(c *gin.Context) {
	pkg := lms_rest.SubPackage{}
	err := c.ShouldBind(&pkg)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": 500, "msg": "数据格式错误", "error_detail": err.Error()})
		return
	}
	var alg encrypt.EncryptAlg
	alg, err = common.GetEncryptAlg(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": 500, "msg": "加密算法未初始化", "error_detail": "null EncryptAlg"})
		return
	}
	err = lms_rest.Verify(alg, &pkg)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": 500, "msg": "授权包不合法", "error_detail": err.Error()})
		return
	}
	if lms_rest.Exists(c, &pkg) {
		c.JSON(http.StatusNoContent, gin.H{"code": 204, "msg": "授权包重复导入", "error_detail": "授权包重复导入"})
		return
	}
	err = lms_packages.ImportSubpackage(c, pkg)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": 500, "msg": "授权包导入失败", "error_detail": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"code": 200, "msg": ""})
}
