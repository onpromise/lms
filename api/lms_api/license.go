package lms_api

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"gitee.com/onpromise/lms/api/api_common"
	"gitee.com/onpromise/lms/consts"
	"gitee.com/onpromise/lms/models/db/lms_db"
	"gitee.com/onpromise/lms/models/rest/lms_rest"
	"gitee.com/onpromise/lms/utils"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/jinzhu/copier"
	"gorm.io/gorm"
)

func licenseApply(c *gin.Context) {
	var err error
	var duration time.Duration
	err_msg := ""
	ctxdb := c.Value(consts.CTXDB).(*gorm.DB)
	rest_license := lms_rest.ApplyRequest{}
	dbLicense := lms_db.License{}
	licenseCounts := []lms_db.LicenseCount{}
	now := time.Now().UTC()
	err = c.ShouldBind(&rest_license)
	if err != nil {
		err_msg = "请求数据解析失败"
		goto err_proc
	}
	err = ctxdb.Find(&licenseCounts).Error
	if err != nil {
		err_msg = "查询数据失败,这只是一个测试"
		goto err_proc
	}
	copier.Copy(&dbLicense, &rest_license)
	dbLicense.LicenseId = uuid.New().String()
	duration, err = utils.ParseDuration(rest_license.Duration)
	if err != nil {
		err_msg = "申请授权的时间格式有误"
		goto err_proc
	}
	dbLicense.ExpiredAt = now.Add(duration).Format(time.RFC3339)
	dbLicense.AssignedAt = now.Format(time.RFC3339)
	err = ctxdb.Create(&dbLicense).Error
	if err != nil {
		err_msg = "创建授权失败"
		goto err_proc
	}
	c.JSON(http.StatusOK, gin.H{"code": 0, "msg": "success", "data": gin.H{"license_id": dbLicense.LicenseId}})
err_proc:
	if err != nil {
		api_common.ErrResp(c, http.StatusInternalServerError, err_msg, gin.H{"error_detail": err.Error()})
	}
}
func licenseList(c *gin.Context) {
	var err error
	var err_msg string
	ctxdb := c.Value(consts.CTXDB).(*gorm.DB)
	licenses := []lms_db.License{}
	rest_licenses := []lms_rest.License{}
	if ctxdb == nil {
		err_msg = "无法连接数据库"
		err = fmt.Errorf("get db failed")
		goto err_proc
	} else {
		err = ctxdb.Find(&licenses).Error
	}
	if err != nil {
		err_msg = "数据查询失败"
		goto err_proc
	}
	copier.Copy(&rest_licenses, &licenses)
	c.JSON(http.StatusOK, gin.H{"code": 0, "msg": "success", "data": gin.H{"licenses": rest_licenses}})
err_proc:
	if err != nil {
		api_common.ErrResp(c, http.StatusInternalServerError, err_msg, gin.H{"error_detail": err.Error()})
	}
}
func licenseRevert(c *gin.Context) {
	var err error
	var err_msg string
	ctxdb := c.Value(consts.CTXDB).(*gorm.DB)
	licenseid := c.Param("licenseid")
	license := lms_db.License{}
	if ctxdb == nil {
		err_msg = "无法连接数据库"
		err = fmt.Errorf("get db failed")
		goto err_proc
	}
	ctxdb.Model(&lms_db.License{}).Where("license_id = ?", licenseid).Delete(&lms_db.License{})
	ctxdb.Model(&lms_db.License{}).Where("license_id = ?", licenseid).First(&license)
	if license.LicenseId == licenseid {
		err_msg = "删除失败"
		err = fmt.Errorf("delete license failed:%s", licenseid)
		goto err_proc
	}
	c.JSON(http.StatusOK, gin.H{"code": 0, "msg": "success", "data": gin.H{}})
err_proc:
	if err != nil {
		api_common.ErrResp(c, http.StatusInternalServerError, err_msg, gin.H{"error_detail": err.Error()})
	}
}
func licenseUpdate(c *gin.Context) {
	var err error
	var err_msg string
	var duration time.Duration
	var t time.Time
	licenseid := c.Param("licenseid")
	rest_license := lms_rest.ApplyRequest{}
	license := lms_db.License{}
	ctxdb := c.Value(consts.CTXDB).(*gorm.DB)
	err = c.ShouldBind(&rest_license)
	if err != nil {
		err_msg = "请求数据解析错误"
		goto err_proc
	}
	if ctxdb == nil {
		err_msg = "无法连接数据库"
		err = fmt.Errorf("get db failed")
		goto err_proc
	}
	err = ctxdb.Model(&lms_db.License{}).Where("license_id = ?", licenseid).First(&license).Error
	if err != nil {
		err_msg = "无法获取授权,更新失败"
		goto err_proc
	}
	duration, err = utils.ParseAppendDuration(rest_license.Duration)
	if err != nil {
		err_msg = "追加时间格式错误"
		goto err_proc
	}
	t, _ = time.Parse(time.RFC3339, license.ExpiredAt)
	t = t.Add(duration)
	license.ExpiredAt = t.Format(time.RFC3339)
	if rest_license.Spec != "" {
		license.Spec = rest_license.Spec
	}
	err = ctxdb.Save(&license).Error
	if err != nil {
		err_msg = "数据更新失败"
		goto err_proc
	}
	c.JSON(http.StatusOK, gin.H{"code": 0, "msg": "success", "data": gin.H{}})
err_proc:
	if err != nil {
		api_common.ErrResp(c, http.StatusInternalServerError, err_msg, gin.H{"error_detail": err.Error()})
	}
}
func licenseGet(c *gin.Context) {
	var err error
	var err_msg string
	ctxdb := c.Value(consts.CTXDB).(*gorm.DB)
	license := lms_db.License{}
	licenses := []lms_db.License{}
	rest_license := lms_rest.License{}
	licenseid := c.Param("licenseid")
	if ctxdb == nil {
		err_msg = "无法连接数据库"
		err = fmt.Errorf("get db failed")
		goto err_proc
	}
	ctxdb.Find(&licenses)
	log.Println(licenses)
	err = ctxdb.Model(&lms_db.License{}).Where("license_id = ?", licenseid).First(&license).Error
	if err != nil {
		err_msg = "数据查询失败"
		goto err_proc
	}
	copier.Copy(&rest_license, &license)
	c.JSON(http.StatusOK, gin.H{"code": 0, "msg": "success", "data": rest_license})
err_proc:
	if err != nil {
		api_common.ErrResp(c, http.StatusInternalServerError, err_msg, gin.H{"error_detail": err.Error()})
	}
}
