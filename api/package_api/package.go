package package_api

import (
	"net/http"

	"gitee.com/onpromise/lms/common"
	"gitee.com/onpromise/lms/lib/encrypt"
	"gitee.com/onpromise/lms/lms_packages"
	"gitee.com/onpromise/lms/models/rest/lms_rest"

	"github.com/gin-gonic/gin"
)

//1. 验证总包自己是否合法, 如果之前已经导入过就返回
//2. 向各个signer验证subpackage是否合法, 如果之前导入过, 返回错误
//3. 导入总包信息
//4. 导入各个子包
func importPackage(c *gin.Context) {
	pkg := lms_rest.LMSPackage{}
	err := c.ShouldBind(&pkg)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": 500, "msg": "数据格式错误", "error_detail": err.Error()})
		return
	}
	var alg encrypt.EncryptAlg
	alg, err = common.GetEncryptAlg(c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": 500, "msg": "加密算法未初始化", "error_detail": "null EncryptAlg"})
		return
	}
	err = lms_rest.Verify(alg, &pkg)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": 500, "msg": "授权包不合法", "error_detail": err.Error()})
		return
	}
	if lms_rest.Exists(c, &pkg) {
		c.JSON(http.StatusNoContent, gin.H{"code": 204, "msg": "授权包重复导入", "error_detail": "授权包重复导入"})
		return
	}
	err = lms_packages.Import(c, pkg)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": 500, "msg": "授权包导入失败", "error_detail": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"code": 200, "msg": ""})
}
