package lms_db

import "gorm.io/gorm"

type LicenseCount struct {
	gorm.Model
	Assigned        int    `json:"assigned,omitempty" yaml:"assigned,omitempty"`
	Available       int    `json:"available,omitempty" yaml:"available,omitempty"`
	Expired         int    `json:"expired,omitempty" yaml:"expired,omitempty"`
	ExpiredIn2Month int    `json:"expired_in_2_month,omitempty" yaml:"expired_in_2_month,omitempty"`
	Failed          int    `json:"failed,omitempty" yaml:"failed,omitempty"`
	Reverted        int    `json:"reverted,omitempty" yaml:"reverted,omitempty"`
	Signer          string `json:"signer,omitempty" yaml:"signer,omitempty"`
	SignerType      string `json:"signer_type,omitempty"`
	Total           int    `json:"total,omitempty" yaml:"total,omitempty"`
}
type LicenseCounts []LicenseCount

func (cs LicenseCounts) Copy(cb func(LicenseCount)) {
	for _, item := range cs {
		cb(item)
	}
}
func (cs LicenseCounts) Sum() LicenseCount {
	totalCount := LicenseCount{}
	for _, item := range cs {
		totalCount.Assigned += item.Assigned
		totalCount.Available += item.Available
		totalCount.Expired += item.Expired
		totalCount.ExpiredIn2Month += item.ExpiredIn2Month
		totalCount.Failed += item.Failed
		totalCount.Reverted += item.Reverted
		totalCount.Total += item.Total
	}
	return totalCount
}
