package serialnum

import (
	"testing"

	"github.com/wI2L/jsondiff"
)

func TestAppend(t *testing.T) {
	sn := NewSerialNum()
	sn.Append("123456")
	if sn.Count() != 1 {
		t.Log(sn.String())
		t.Fatal("serialNum append test failed")
	}
	sn.Append("456789")
	if sn.Count() != 2 {
		t.Fatal("serialNum append test failed")
	}
	if sn.Slice()[0] != "123456" {
		t.Fatal("serialNum append test failed")
	}
	if sn.Slice()[1] != "456789" {
		t.Fatal("serialNum append test failed")
	}
}
func TestHardwareInfos(t *testing.T) {
	h := hardware{}
	infos := h.GetHardwareInfos()
	for i := 0; i < 1000; i++ {
		h2 := hardware{}
		infos2 := h2.GetHardwareInfos()
		patch, err := jsondiff.Compare(infos, infos2)
		if len(patch) != 0 || err != nil {
			t.Fatal("infos changed")
		}
	}
}
