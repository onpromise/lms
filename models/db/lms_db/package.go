package lms_db

import (
	"database/sql/driver"
	"encoding/json"
	"time"

	"gitee.com/onpromise/lms/consts"
	"gitee.com/onpromise/lms/models/rest/lms_rest"
	"gorm.io/gorm"
)

type LMSPackage lms_rest.LMSPackage
type LMSSubPackage lms_rest.SubPackage
type LMSPackageDB struct {
	gorm.Model
	Description string     `gorm:"type:text" json:"description"`
	ImportedAt  time.Time  `json:"imported_at"`
	PackageID   string     `json:"package_id"`
	BlockID     string     `json:"block_id"`
	Package     LMSPackage `gorm:"type:JSON" json:"package"`
}
type LMSSubPackageDB struct {
	gorm.Model
	Description string        `gorm:"type:text" json:"description"`
	ImportedAt  time.Time     `json:"imported_at"`
	PackageID   string        `json:"package_id"`
	BlockID     string        `json:"block_id"`
	Package     LMSSubPackage `gorm:"type:JSON" json:"package"`
}

func (l LMSPackageDB) TableName() string {
	return consts.LMS_PKG_TABLENAME
}

//占用和LMSPackageDB一样的表名, 因为这两个在不同的数据库中
func (l LMSSubPackageDB) TableName() string {
	return consts.LMS_PKG_TABLENAME
}

func (m *LMSPackage) Scan(value interface{}) error {
	switch data := value.(type) {
	case string:
		return json.Unmarshal([]byte(data), m)
	}
	return json.Unmarshal(value.([]byte), m)
}
func (m LMSPackage) Value() (driver.Value, error) {
	return json.Marshal(m)
}
func (m *LMSSubPackage) Scan(value interface{}) error {
	switch data := value.(type) {
	case string:
		return json.Unmarshal([]byte(data), m)
	}
	return json.Unmarshal(value.([]byte), m)
}
func (m LMSSubPackage) Value() (driver.Value, error) {
	return json.Marshal(m)
}
func (l *LMSPackageDB) ValidFoundationBlockTime(db *gorm.DB, createAt time.Time) bool {
	lmsDBS := []LMSPackageDB{}
	if err := db.Find(&lmsDBS).Error; err != nil {
		return false
	}
	//如果还没有导入包, 就认为合法
	if len(lmsDBS) <= 0 {
		return true
	}
	v := lmsDBS[0].ImportedAt.Sub(createAt)
	//导入包时间早于FoundationBlock一个小时以上
	if v < -1*time.Hour {
		return false
	}
	return true
}
