//验证gin的认证授权中间件
//顺便搭建基本的接口测试框架, 其他测试借鉴这个代码
package lms

import (
	"encoding/base64"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
)

func authorizationHeader(user, password string) string {
	base := user + ":" + password
	return "Basic " + base64.StdEncoding.EncodeToString([]byte(base))
}

func TestBasicAuth(t *testing.T) {
	// t.Fatal("not implemented")
	r := gin.Default()
	r.Use(gin.BasicAuth(gin.Accounts{
		"demo": "demopwd",
	}))
	authorized := r.Group("/")
	authorized.GET("/api/version", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"Version": "0.0.1"})
	})
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/version", nil)
	req.Header.Set("Authorization", authorizationHeader("demo", "demopwd"))
	r.ServeHTTP(recorder, req)
	if recorder.Code != http.StatusOK {
		t.Fatal("auth test failed")
	}
	req2, _ := http.NewRequest("GET", "/api/version", nil)
	recorder2 := httptest.NewRecorder()
	req2.Header.Set("Authorization", authorizationHeader("demo", "wrongpass"))
	r.ServeHTTP(recorder2, req2)
	if recorder2.Code == http.StatusOK {
		t.Fatal("auth test failed")
	}
}
