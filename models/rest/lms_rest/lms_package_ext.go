package lms_rest

import (
	"context"
	"encoding/json"
	"fmt"

	"gitee.com/onpromise/lms/common"
	"gitee.com/onpromise/lms/consts"
	"gitee.com/onpromise/lms/lib/encrypt"
	"github.com/lwydyby/logrus"
	"gorm.io/gorm"
)

type PackageVerify interface {
	VerifiedData() []byte
	VerifiedSignature() string
	GetPackageID() string
}
type DummyPackage struct {
	gorm.Model
	PackageID string `json:"package_id"`
}

func (d DummyPackage) TableName() string {
	return consts.LMS_PKG_TABLENAME
}

func (l *LMSPackage) VerifiedData() []byte {
	m := map[string]interface{}{}
	bs, err := json.Marshal(l)
	if err != nil {
		logrus.Error(err)
		return []byte{}
	}
	err = json.Unmarshal(bs, &m)
	delete(m, "signature")
	bs, _ = json.Marshal(m)
	return bs
}
func (l *LMSPackage) VerifiedSignature() string {
	return l.Signature
}
func (l *LMSPackage) GetPackageID() string {
	return l.PackageId
}
func (l *SubPackage) VerifiedData() []byte {
	m := map[string]interface{}{}
	bs, err := json.Marshal(l)
	if err != nil {
		logrus.Error(err)
		return []byte{}
	}
	err = json.Unmarshal(bs, &m)
	delete(m, "signature")
	bs, _ = json.Marshal(m)
	return bs
}
func (l *SubPackage) VerifiedSignature() string {
	return l.Signature
}
func (l *SubPackage) GetPackageID() string {
	return l.PackageId
}
func Verify(alg encrypt.EncryptAlg, pkg PackageVerify) error {
	data := pkg.VerifiedData()
	signature := pkg.VerifiedSignature()
	err := alg.VerifySign(data, signature)
	if err != nil {
		logrus.Error(err)
	}
	return err
}
func Exists(ctx context.Context, pkg PackageVerify) bool {
	db, err := common.GetCtxDB(ctx)
	if err != nil {
		logrus.Error(err)
		return false
	}
	null := DummyPackage{}
	db.Where("package_id = ?", pkg.GetPackageID()).Find(&null)
	if null.PackageID == pkg.GetPackageID() {
		err = fmt.Errorf("重复导入包")
		logrus.WithField("package_id", pkg.GetPackageID()).Error(err)
		return true
	}
	return false
}
