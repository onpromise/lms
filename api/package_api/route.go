package package_api

import "github.com/gin-gonic/gin"

func Routes(r *gin.RouterGroup) *gin.RouterGroup {
	r.GET("/packages", func(c *gin.Context) {})             //获取所有已经导入的授权包的信息
	r.POST("/packages", importPackage)                      //导入新的授权包
	r.PUT("/packages/:package_id", func(c *gin.Context) {}) //更新授权包
	return r
}
