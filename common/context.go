package common

import (
	"context"
	"fmt"

	"gitee.com/onpromise/lms/consts"
	"gitee.com/onpromise/lms/lib/encrypt"
	"gorm.io/gorm"
)

func GetEncryptAlg(ctx context.Context) (encrypt.EncryptAlg, error) {
	ctx_v := ctx.Value(consts.ENCRYTP_ALG)
	if ctx_v != nil {
		alg := ctx_v.(encrypt.EncryptAlg)
		return alg, nil
	}
	return nil, fmt.Errorf("null encrypt alg")
}
func GetCtxDB(ctx context.Context) (*gorm.DB, error) {
	ctx_v := ctx.Value(consts.CTXDB)
	if ctx_v != nil {
		return ctx_v.(*gorm.DB), nil
	}
	return nil, fmt.Errorf("null gormdb")
}
